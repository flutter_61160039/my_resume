import 'package:flutter/material.dart';

Row _buildPersonalData(IconData icon, String head, String data) {
  return Row(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(
        icon,
        color: Colors.black,
      ),
      Container(
        padding: const EdgeInsets.only(left: 10),
        child: Text(
          head,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
      Expanded(
          child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            padding: EdgeInsets.only(left: 0),
            child: Text(
              data,
              maxLines: 1,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
        ],
      )),
    ],
  );
}
Row _buildTitle(IconData icon, String label) {
  return Row(mainAxisSize: MainAxisSize.min, children: [
    Icon(
      icon,
      color: Colors.black,
    ),
    Expanded(
      child: Container(
        padding: const EdgeInsets.only(left: 10),
        child: Text(
          label,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
    )
  ]);
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
        padding: const EdgeInsets.all(32),
        child: Column(
          children: [
            Container(
              child: _buildPersonalData(
                  Icons.account_box, 'ชื่อ', 'เมธพนธ์ พ้นภัยพาล'),
            ),
            Container(
              child: _buildPersonalData(
                  Icons.cake, 'วันเกิด', '5 เมษายน พ.ศ.2543'),
            ),
            Container(
              child: _buildPersonalData(Icons.male, 'เพศ', 'ชาย'),
            ),
            Container(
              child: _buildPersonalData(Icons.home, 'ที่อยู่',
                  '44/1 หมู่ 5 ต.เหมือง อ.เมือง จ.ชลบุรี 20130'),
            ),
            Container(
              child: _buildPersonalData(
                  Icons.email, 'Email', 'Jamesmetapon@hotmail.com'),
            ),
            Container(
              child: _buildPersonalData(Icons.phone, 'เบอร์โทร', '098-8291385'),
            ),
          ],
        ));
    Widget skillSection = Container(
      padding: const EdgeInsets.fromLTRB(32, 0, 0, 0),
      child: _buildTitle(Icons.computer, 'ทักษะ'),
    );
    Widget imageListSection = Container(
        padding: const EdgeInsets.fromLTRB(0, 0, 0, 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              'images/java.png',
              height: 75,
              fit: BoxFit.contain,
            ),
            Image.asset(
              'images/C#.png',
              height: 75,
              fit: BoxFit.contain,
            ),
            Image.asset(
              'images/python.png',
              height: 75,
              fit: BoxFit.contain,
            ),
            Image.asset(
              'images/C++.png',
              height: 75,
              fit: BoxFit.contain,
            ),
          ],
        ));
    Widget educationSection = Container(
      padding: const EdgeInsets.fromLTRB(32, 0, 0, 10),
      child: _buildTitle(Icons.school, 'การศึกษา'),
    );
    Widget educationDataSection = Container(
      padding: const EdgeInsets.fromLTRB(32, 0, 32, 0),
      child: Column(
        children: [
          Container(child: _buildPersonalData(Icons.groups, 'การศึกษาระดับปริญญาตรี', 'มหาวิทยาลัยบูรพา'),),
          Container(child: _buildPersonalData(Icons.groups, 'การศึกษาระดับมัธยมศึกษา', 'โรงเรียนชลราษฎรอํารุง'),)
        ],
      )
    );
    return MaterialApp(
        title: 'My Resume',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('My Resume'),
          ),
          body: ListView(
            children: <Widget>[
              Image.asset(
                'images/Metapon.jpg',
                height: 240,
                fit: BoxFit.fitHeight,
              ),
              titleSection,
              skillSection,
              imageListSection,
              educationSection,
              educationDataSection
            ],
          ),
        ));
  }
}
